var myIp     = require("my-local-ip");
var document = {
    production : {
        notification: {
            ios    : {
                brandCertPath: '../cert/Peberry_Brand.p12',
                userCertPath : '../cert/Peberry_User.p12',
                passphrase   : '1',
                mode         : 'production'
            },
            android: {
                USER_GCM_API_KEY : 'AIzaSyCtQoPEFHAZc4kfqouy_5ViMEMCV1leD0c',
                BRAND_GCM_API_KEY: 'AIzaSyBezawrJlVttDA208FSplSEzLcC0mdKYs8'
            }
        },
        database    : {
            "couchbase"  : {
                "userBrand" : {
                    host      : "cb.peberry.co",
                    port      : "8091",
                    bucketName: "userBrand"
                },
                "postReward": {
                    host      : "cb.peberry.co",
                    port      : "8091",
                    bucketName: "postReward"
                },
                "points"    : {
                    host      : "cb.peberry.co",
                    port      : "8091",
                    bucketName: "points"
                }
            },
            elasticsearch: {
                host: 'es.peberry.co:9200'
            },
            "redis"      : {
                host: "rd.peberry.co",
                port: 6379
            }
        },
        "hmqServer" : {
            hosts: ["api.peberry.co:8000"]
        },
        "chat"      : {
            host         : 'chat.peberry.co',
            /*port         : 5000,*/
            protocol     : 'http',
            authorization: '10061v-e2ib-77r2-j8a2-92i1d080nVK2'
        }
    },
    stage      : {
        notification: {
            ios    : {
                brandCertPath: '../cert/Peberry_Brand.p12',
                userCertPath : '../cert/Peberry_User.p12',
                passphrase   : '1',
                mode         : 'sandbox'
            },
            android: {
                USER_GCM_API_KEY : 'AIzaSyCtQoPEFHAZc4kfqouy_5ViMEMCV1leD0c',
                BRAND_GCM_API_KEY: 'AIzaSyBezawrJlVttDA208FSplSEzLcC0mdKYs8'
            }
        },
        database    : {
            "couchbase"  : {
                "userBrand" : {
                    host      : "cb.peberry.ml",
                    port      : "8091",
                    bucketName: "userBrand"
                },
                "postReward": {
                    host      : "cb.peberry.ml",
                    port      : "8091",
                    bucketName: "postReward"
                },
                "points"    : {
                    host      : "cb.peberry.ml",
                    port      : "8091",
                    bucketName: "points"
                }
            },
            elasticsearch: {
                host: 'es.peberry.ml:9200'
            },
            "redis"      : {
                host: "rd.peberry.ml",
                port: 6379
            }
        },
        "hmqServer" : {
            hosts: ["api.peberry.ml:8000"]
        },
        "chat"      : {
            host         : 'ch.liad.in',
            port         : 5000,
            protocol     : 'http',
            authorization: '5530616p-b2ne-44b2-r9r1-96a8d690yPe2'
        }
    },
    development: {
        notification: {
            ios    : {
                brandCertPath: '../cert/Peberry_Brand.p12',
                userCertPath : '../cert/Peberry_User.p12',
                passphrase   : '1',
                mode         : 'sandbox'
            },
            android: {
                USER_GCM_API_KEY : 'AIzaSyCtQoPEFHAZc4kfqouy_5ViMEMCV1leD0c',
                BRAND_GCM_API_KEY: 'AIzaSyBezawrJlVttDA208FSplSEzLcC0mdKYs8'
            }
        },
        database    : {
            "couchbase"  : {
                "userBrand" : {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "userBrand"
                },
                "postReward": {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "postReward"
                },
                "points"    : {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "points"
                }
            },
            elasticsearch: {
                host: ''
            },
            "redis"      : {
                host: "",
                port: 6379
            }
        },
        "hmqServer" : {
            hosts: ["api.stage.peberry.tk:8000"]
        },
        "chat"      : {
            host         : 'ch.liad.in',
            port         : 5000,
            protocol     : 'http',
            authorization: '5530616p-b2ne-44b2-r9r1-96a8d690yPe2'
        }
    },
    default    : {
        notification: {
            ios    : {
                brandCertPath: '../cert/Peberry_Brand.p12',
                userCertPath : '../cert/Peberry_User.p12',
                passphrase   : '1',
                mode         : 'sandbox'
            },
            android: {
                USER_GCM_API_KEY : 'AIzaSyCtQoPEFHAZc4kfqouy_5ViMEMCV1leD0c',
                BRAND_GCM_API_KEY: 'AIzaSyBezawrJlVttDA208FSplSEzLcC0mdKYs8'
            }
        },
        database    : {
            "couchbase"  : {
                "userBrand" : {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "userBrand"
                },
                "postReward": {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "postReward"
                },
                "points"    : {
                    host      : "db.stage.peberry.tk",
                    port      : "8091",
                    bucketName: "points"
                }
            },
            elasticsearch: {
                host: 'es.stage.peberry.tk:9200'
            },
            "redis"      : {
                host: "localhost",
                port: 6379
            }
        },
        "hmqServer" : {
            hosts: ["api.stage.peberry.tk:8000"]
        },
        "chat"      : {
            host         : 'ch.liad.in',
            port         : 5000,
            protocol     : 'http',
            authorization: '5530616p-b2ne-44b2-r9r1-96a8d690yPe2'
        }
    }
};

var loadDocument = null;
var environment  = process.env.MODE;

if (environment == "production") {
    loadDocument = document.production;
} else if (environment == "stage") {
    loadDocument = document.stage;
} else if (environment == "development") {
    loadDocument = document.development;
} else {
    loadDocument = document.default;
}

module.exports = loadDocument;

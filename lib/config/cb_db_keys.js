module.exports = {
    BRAND_PROFILE : "BRD:",
    USER_PROFILE  : "USR:",
    POST          : "POST:",
    REWARD        : "RWD:",
    POINTS_HISTORY: "PT_HST:",
    USER_BRAND_MAP: "USR_BRD_MAP:",
    WALK_INS      : "WLK:",
    AUTH          : "AUTH:",
    EMAIL         : "EMAIL:",
    TWITTER       : "TWITTER:",
    INSTA         : "INSTA:",
    FB            : "FB:",
    FILE          : "FILE:",

    BRAND_EMAIL  : "BRD_EMAIL:",
    BRAND_TWITTER: "BRD_TWITTER:",
    BRAND_INSTA  : "BRD_INSTA:",
    BRAND_FB     : "BRD_FB:",

    SESSION : "SESSION:",
    DEVICE  : "DEVICE:",
    USER_DEV: "USR_DEV:",
    APP_DATA: "APP_DATA",

    CHAT         : "CHAT:",
    CHAT_INITIATE: "CHAT_INIT:",

    ADMIN_EMAIL  : "ADMIN_EMAIL:",
    ADMIN_PROFILE: "ADMIN:",
    BRAND_CREDIT : "BRD_CREDIT:",
    FAIRNESS     : "FAIRNESS:",
    FAIRNESS_LOG : "FAIRNESS_LOG:",

    BRAND_AUTH       : "BRD_AUTH:",
    BRAND_SESSION    : "BRD_SESSION:",
    BRAND_DEVICE     : "BRD_DEVICE:",
    BRAND_USER_DEVICE: "BRD_USR_DEV:",

    USER_AUTH       : "USR_AUTH:",
    USER_SESSION    : "USR_SESSION:",
    USER_DEVICE     : "USR_DEVICE:",
    USER_USER_DEVICE: "USR_USR_DEV:",
    PROMO_CODE      : "PROMO_CODE:",

    ADMIN_AUTH: "ADMIN_AUTH:",

    NOTIFICATION: "NOTI:"
};
module.exports = {
    BRAND_PROFILE : "brandProfile",
    USER_PROFILE  : "userProfile",
    POST          : "post",
    REWARD        : "reward",
    POINTS_HISTORY: "pointsHistory",
    USER_BRAND_MAP: "userBrandMap",
    FAIRNESS      : "fairness",
    FAIRNESS_LOG  : "fairnessLog",
    PROMO_CODE    : "promoCode",
    PEBERRY       : "peberry",
    NOTIFICATION  : "notification"
};
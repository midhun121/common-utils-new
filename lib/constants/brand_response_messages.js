module.exports = {
    RES_SUCCESS                         : {
        "code"   : 200,
        "message": "Success"
    },
    RES_INT_SERVER_ERR                  : {
        "code"   : 500,
        "message": "Internal Server Error."
    },
    RES_NOT_IMPLEMENTED                 : {
        "code"   : 201,
        "message": "Not Implemented Yet."
    },
    RES_BAD_REQUEST                     : {
        "code"   : 400,
        "message": "Bad Request."
    },
    RES_BAD_TOKEN                       : {
        "code"   : 401,
        "message": "Bad Token."
    },
    RES_AUTH_FAILED                     : {
        "code"   : 402,
        "message": "Invalid Password."
    },
    RES_MAIL_ID_EXISTS                  : {
        "code"   : 403,
        "message": "Mail Id Already Registered."
    },
    RES_MAIL_NOT_EXISTS                 : {
        "code"   : 404,
        "message": "Mail Id Not Exists."
    },
    RES_BRAND_NOT_EXISTS                : {
        "code"   : 405,
        "message": "Brand not exists."
    },
    RES_INVALID_CURRENT_PWD             : {
        "code"   : 406,
        "message": "Invalid current password."
    },
    RES_INSUFFICIENT_PTS                : {
        "code"   : 407,
        "message": "Insufficient points to allocate."
    },
    RES_EXCEEDS_REDEMPTION_LIMIT        : {
        "code"   : 408,
        "message": "Exceeds redemption limit."
    },
    RES_INACTIVE_REWARD                 : {
        "code"   : 409,
        "message": "Inactive Reward."
    },
    RES_POST_NOT_EXISTS                 : {
        "code"   : 410,
        "message": "Post not exists."
    },
    RES_INSUFFICIENT_POINT_TO_ADD_REWARD: {
        "code"   : 411,
        "message": "Insufficient points to add new reward."
    },
    RES_REWARD_NOT_EXISTS               : {
        "code"   : 412,
        "message": "Reward not exists."
    },
    RES_REWARD_ALREADY_EXISTS           : {
        "code"   : 413,
        "message": "Reward already exists."
    },
    RES_SPEED_DIAL_ALREADY_ASSIGNED     : {
        "code"   : 414,
        "message": "Speed dial already assigned."
    },
    RES_EXCEEDS_REWARD_LIMIT            : {
        "code"   : 415,
        "message": "Exceeds reward add limit."
    },
    RES_USER_NOT_EXISTS                 : {
        "code"   : 416,
        "message": "User not exists."
    },
    RES_BRAND_INACTIVE                  : {
        "code"   : 417,
        "message": "Brand is in-active."
    },
    RES_SOCIAL_NETWORK_ALREADY_EXISTS   : {
        "code"   : 418,
        "message": "Social ID already exists."
    },
    RES_USER_NOT_EXISTS                 : {
        "code"   : 419,
        "message": "User Not exists."
    },
    RES_INVALID_BEACON_ID               : {
        "code"   : 420,
        "message": "Invalid Beacon Id."
    },
    RES_WALK_IN_NOT_FOUND               : {
        "code"   : 422,
        "message": "WalkIn Not found."
    },
    RES_PROFILE_NOT_COMPLETED           : {
        "code"   : 425,
        "message": "Profile Not Completed."
    },
    RES_PRODUCT_ID_REQUIRED             : {
        "code"   : 426,
        "message": "Product Id Required."
    },
    RES_CANNOT_UPDATE_REWARD_TYPE       : {
        "code"   : 427,
        "message": "Reward Type does not match."
    },
    RES_INVALID_TRY                     : {
        "code"   : 428,
        "message": "Invalid Try."
    },
    RES_ACCOUNT_INACTIVE                : {
        "code"   : 429,
        "message": "Your Account is Inactive."
    },
    RES_PRODUCT_NOT_EXISTS              : {
        "code"   : 430,
        "message": "Product Not Exists."
    },
    RES_ALLOCATED_POINTS_EXCEEDS        : {
        "code"   : 431,
        "message": "Allocated Points Exceeds the Maximum Limit."
    },
    RES_REDEEM_POINTS_EXCEEDS           : {
        "code"   : 432,
        "message": "Redeem Points Exceeds the Maximum Limit."
    },
    RES_FILE_NOT_FOUND                  : {
        "code"   : 450,
        "message": "File Not Found."
    },
    RES_USER_NOT_FOUND                  : {
        "code"   : 451,
        "message": "User Not Found."
    },
    RES_ALREADY_REPORTED                : {
        "code"   : 453,
        "message": "You have Already reported this."
    },
    RES_EMAIL_ALREADY_REGISTERED        : {
        "code"   : 456,
        "message": "Mail Id Already Registered."
    },
    RES_MAXIMUM_PROMO_CODE_USAGE        : {
        "code"   : 457,
        "message": "You've reached the maximum number of Promo Code Usage."
    },
    RES_STORE_NOT_AVAILABLE             : {
        "code"   : 458,
        "message": "Please contact the Brand to Checkin to the store."
    },
    RES_CUSTOM_ERROR                    : function (message) {
        return {"code": 500, "message": message}
    }
};
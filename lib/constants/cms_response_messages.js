module.exports = {
    RES_SUCCESS                  : {
        "code"   : 200,
        "message": "Success"
    },
    RES_INT_SERVER_ERR           : {
        "code"   : 500,
        "message": "Internal Server Error"
    },
    RES_NOT_IMPLEMENTED          : {
        "code"   : 201,
        "message": "Not Implemented Yet"
    },
    RES_BAD_REQUEST              : {
        "code"   : 400,
        "message": "Bad Request"
    },
    RES_BAD_TOKEN                : {
        "code"   : 401,
        "message": "Bad Token"
    },
    RES_AUTH_FAILED              : {
        "code"   : 402,
        "message": "Invalid Password"
    },
    RES_MAIL_NOT_EXISTS          : {
        "code"   : 403,
        "message": "Mail Id Not Exists"
    },
    RES_INVALID_CREDENTIALS      : {
        "code"   : 404,
        "message": "Invalid credentials"
    },
    RES_INVALID_CURRENT_PWD      : {
        "code"   : 405,
        "message": "Invalid current password"
    },
    RES_BRAND_NOT_EXISTS         : {
        "code"   : 406,
        "message": "Brand not exists"
    },
    RES_INACTIVE_ACCOUNT         : {
        "code"   : 407,
        "message": "Your Account is Inactive"
    },
    RES_USER_NOT_EXISTS          : {
        "code"   : 408,
        "message": "User not Exists"
    },
    RES_PERMISSION_DENIED        : {
        "code"   : 408,
        "message": "Permission Denied for your Role"
    },
    RES_REWARD_EXISTS_FOR_PRODUCT: {
        "code"   : 409,
        "message": "Unable to delete as there are active rewards available"
    },
    RES_POST_NOT_EXISTS          : {
        "code"   : 410,
        "message": "Post not exists"
    },
    RES_FILE_NOT_FOUND           : {
        "code"   : 450,
        "message": "File Not Found"
    }
};
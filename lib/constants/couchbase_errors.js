var couchbase = require('couchbase');

/**
 * @type {{KEY_ALREADY_EXISTS: (exports|number), KEY_NOT_FOUND: (exports|number)}}
 */
module.exports = {
	KEY_ALREADY_EXISTS : couchbase.errors.keyAlreadyExists,
	KEY_NOT_FOUND : couchbase.errors.keyNotFound
};
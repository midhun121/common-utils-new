var joi = require("joi");

module.exports = {
    id           : joi.string().required(),
    dbType       : joi.string().default('admin'),
    email        : joi.string().required(),
    password     : joi.string().required(),
    name         : joi.object({
        firstName: joi.string().required(),
        lastName : joi.string().required()
    }).required(),
    role         : joi.string().required().valid(["SUPER_ADMIN"]),
    createdDate  : joi.date().required(),
    updatedDate  : joi.date(),
    lastLoggedIn : joi.date(),
    resetPassword: joi.object({
        password: joi.string().required(),
        date    : joi.date().required()
    }),
    status       : joi.string().required().valid(["ACTIVE", "INACTIVE"])
};
var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("adminEmail"),
    id         : joi.string().description("Unique id of the document. That is same as email Id").required(),
    adminId    : joi.string().description("Unique id for the user").required(),
    isActive   : joi.boolean().required(),
    createdDate: joi.string().required()
};
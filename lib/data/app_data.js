var joi = require("joi");

module.exports = {
    dbType      : joi.string().default("appData"),
    products    : joi.object(),
    /*product     : joi.array().items(joi.object({
     id          : joi.string().required(),
     name        : joi.string().required(),
     label       : joi.string().required(),
     marketPrice : joi.number().required(),
     maxAllocated: joi.number().required().default(10),
     maxRedeemed : joi.number().required().default(100),
     status      : joi.string().required().valid(['ACTIVE', 'INACTIVE', 'DELETED']),
     createdOn   : joi.date().required(),
     updatedOn   : joi.date()
     })),*/
    invitePoints: joi.number().required().default(500)
}
/**
 * Created by jaya on 30/05/16.
 */

var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("authentication"),
    id         : joi.string().description("Unique document id. Same as auth token").required(),
    userType   : joi.string().valid(['USER', 'BRAND', 'ADMIN']),
    createdDate: joi.date().required(),
    updatedDate: joi.date(),
    user       : joi.object({
        id         : joi.string().required().description('user id'),
        mode       : joi.string().valid(["IOS", "WEB", "ANDROID"]),
        deviceId   : joi.string(),
        deviceToken: joi.string()
    })
};
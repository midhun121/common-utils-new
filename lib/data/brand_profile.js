var joi = require("joi");

module.exports = {
    id                       : joi.string().required(),
    dbType                   : joi.string().default('brandProfile'),
    beaconId                 : joi.array().items(joi.string()),
    businessName             : joi.string(),
    website                  : joi.string(),
    email                    : joi.string(),
    telephone                : joi.string(),
    openHours                : joi.string(),
    typeOfBusiness           : joi.string(),
    desc                     : joi.string(),
    location                 : joi.object({
        address    : joi.string().required(),
        geoLocation: joi.object({
            lat: joi.number(),
            lon: joi.number()
        })
    }),
    facebook                 : joi.object({
        fbId    : joi.string().required(),
        fbToken : joi.string().required(),
        email   : joi.string(),
        username: joi.string()
    }),
    twitter                  : joi.object({
        twitterId   : joi.string().required(),
        twitterToken: joi.string().required(),
        email       : joi.string(),
        username    : joi.string()
    }),
    instagram                : joi.object({
        instagramId   : joi.string(),
        instagramToken: joi.string(),
        email         : joi.string(),
        username      : joi.string()
    }),
    mail                     : joi.object({
        email   : joi.string().required(),
        password: joi.string().required()
    }),
    images                   : joi.array().items(joi.string().description("Image Key")),
    thumbnailImageKey        : joi.string().description("Thumbnail Image Key"),
    status                   : joi.string().required().valid(["ACTIVE", "INACTIVE", "DELETED"]),
    sharedPoints             : joi.number().required().default(0),
    socialBrandPoints        : joi.number().required().default(0),
    allocatedPoints          : joi.number().required().default(0),
    redeemedPoints           : joi.number().required().default(0),
    openingBalance           : joi.number().default(0),
    openingSocialBrandBalance: joi.number().default(0),

    sharedAllocatedPoints: joi.number().default(0),
    socialAllocatedPoints: joi.number().default(0),
    brandAllocatedPoints : joi.number().default(0),
    sharedRedeemedPoints : joi.number().default(0),
    socialRedeemedPoints : joi.number().default(0),
    brandRedeemedPoints  : joi.number().default(0),

    credit            : joi.number().default(0).description('Credit Value'),
    creditValue       : joi.number().default(0).description('Credit Value including Price'),
    totalPaidAmount   : joi.number().default(0).description('Total price'),
    paidAmountHistory : joi.array().items(joi.number().default(0)).description('History of Total price'),
    balancePoints     : joi.number().default(0).description('Final Balance Point'),
    adminPoints       : joi.number().default(0).description('balance points added by admin'),
    adminPointsHistory: joi.array().items(joi.number()).description('History of balance points added by admin'),

    createdDate              : joi.date().required(),
    updatedDate              : joi.date(),
    deletedDate              : joi.date(),
    setupCompleted           : joi.boolean().default(false),
    facebookUrl              : joi.string(),
    twitterUrl               : joi.string(),
    instagramUrl             : joi.string(),
    notificationDisabledUsers: joi.array().items(joi.string())
};
/**
 * Created by hmspl on 28/3/16.
 */
var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("chat"),
    id         : joi.string().description("same as user id").required(),
    userId     : joi.string().required().description('Chat user id'),
    createdDate: joi.date()
};

/**
 * Created by hmspl on 29/3/16.
 */

var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("chatInitiate"),
    id         : joi.string().description("same as user id").required(),
    createdDate: joi.date(),
    data       : joi.object()

    /** object will be look like this

     data: {
        $$userId$$: {
            userId: "****",
            chatId: "######",
            date :
        }
    }
     */
};

/**
 * Created by jaya on 30/05/16.
 */

var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("device"),
    deviceType : joi.string().default("IOS").valid(["IOS", "WEB", "ANDROID"]).required(),
    id         : joi.string().description("Unique device id with type").required(),
    userId     : joi.string().description("UserId").required(),
    deviceId   : joi.string().description("Device Id").required(),
    deviceToken: joi.string().description("Device Token"),
    createdDate: joi.date().required(),
    updatedDate: joi.date()
};
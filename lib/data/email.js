var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("email"),
    id         : joi.string().description("Unique id of the document. That is same as email Id").required(),
    userId     : joi.string().description("Unique id for the user").required(),
    isActive   : joi.boolean().required(),
    createdDate: joi.string().required(),
    updatedDate: joi.string()
};

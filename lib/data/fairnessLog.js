/**
 * Created by hmspl on 4/8/16.
 */
var joi = require('joi');

module.exports = {
    "dbType"                       : joi.string().default("fairness"),
    "id"                           : joi.string(),
    "openingBalance"               : joi.number(),
    "rewardId"                     : joi.string(),
    "productId"                    : joi.string(),
    "brandId"                      : joi.string(),
    "isConsidered"                 : joi.boolean(),
    "status"                       : joi.string(),
    "allocatedCount"               : joi.number(),
    "redeemedCount"                : joi.number(),
    "price"                        : joi.number(),
    "allocated"                    : joi.number(),
    "redeemed"                     : joi.number(),
    "allocatedValue"               : joi.number(),
    "redeemedValue"                : joi.number(),
    "volume"                       : joi.number(),
    "weightedAgainstPrice"         : joi.number(),
    "totalPointsAllocated"         : joi.number(),
    "totalPointsAllocatedToPeberry": joi.number(),
    "totalPointsRedeemed"          : joi.number(),
    "totalPointsRedeemedToPeberry" : joi.number(),
    "netPoints"                    : joi.number(),
    "rrpNetPoints"                 : joi.number(),
    "adjustedNetValue"             : joi.number(),
    "currentBalance"               : joi.number(),
    "createdOn"                    : joi.date(),
    "updatedOn"                    : joi.date(),
    "avgPBap"                      : joi.number(),
    "hiddenValue"                  : joi.number(),
    "finalAvgPBap"                 : joi.number(),
    "updatedFairnessValue"         : joi.number(),
    "finalFairnessValue"           : joi.number(),
    "credit"                       : joi.number(),
    "differenceInAllocation"       : joi.number(),
    "creditBalanceWithRRP"         : joi.number(),
    "creditBalance"                : joi.number(),
    "differenceInRRP"              : joi.number(),
    "differenceInRRPAgianstPBaP"   : joi.number()
};
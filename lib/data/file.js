/**
 * Created by jaya on 25/08/15.
 */

var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("file"),
    id         : joi.string().description("Unique document id").required(),
    path       : joi.string().required().description("Path "),
    createdBy  : joi.string(),
    name       : joi.string(),
    fullName   : joi.string(),
    caption    : joi.string(),
    ext        : joi.string().required().description('png, jpeg, gif, mp4'),
    mime       : joi.string().description('mime type. eg image/jpg'),
    iconType   : joi.string().description('Type of the file. eg image, video, pdf'),
    accessType : joi.string().description('public , private'),
    sizeInBytz : joi.number(),
    sizeInStr  : joi.string(),
    createdDate: joi.date().required(),
    updatedDate: joi.date(),
    verified   : joi.boolean().default(false)
};

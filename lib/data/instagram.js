/**
 * Created by hmspl on 21/3/16.
 */
var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("instagram"),
    id         : joi.string().description("Unique id of the document. That is same as fb user Id").required(),
    userId     : joi.string().description("Unique id for the user").required(),
    createdDate: joi.string().required(),
    updatedDate: joi.string()
};

/**
 * Created by hmspl on 3/10/16.
 */
var joi = require("joi");

module.exports = {
    dbType   : joi.string().default("brandNotification").valid(["brandNotification", "userNotification"]),
    id       : joi.string().required(),
    userId   : joi.string().required(),
    brandId  : joi.string().required(),
    //rewardId  : joi.string(),
    notiType : joi.string().valid(["REDEEM_POINTS", "REDEEM_REWARD"]),
    message  : joi.string().required(),
    isRead   : joi.boolean().default(false),
    createdOn: joi.date().required(),
    updatedOn: joi.date(),
    status   : joi.string().valid(["ACTIVE", "DELETED"]).default("ACTIVE")
};
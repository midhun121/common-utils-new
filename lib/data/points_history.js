var joi = require("joi");

module.exports = {
    dbType     : joi.string().default("pointsHistory"),
    id         : joi.string().required(),
    userId     : joi.string().required(),
    brandId    : joi.string(),
    rewardId   : joi.string(),
    quantity   : joi.number().default(1),
    points     : joi.number().required(),
    extraPoints: joi.number().default(0),
    type       : joi.string().required().valid(["ALLOCATED", "REDEEMED", "INVITATION", "INSTALLATION"]),
    createdOn  : joi.date().required(),
    rewardType : joi.string().valid(["SHARED", "BRAND", "SOCIAL"])
}
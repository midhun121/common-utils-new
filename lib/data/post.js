var joi = require("joi");

module.exports = {
    id                : joi.string().required(),
    dbType            : joi.string().default("post"),
    brandId           : joi.string().required(),
    postMessage       : joi.string().required(),
    imageKey          : joi.string().description("Image Key"),
    status            : joi.string().required().valid(["ACTIVE", "INACTIVE", "DELETED", "BLOCKED"]),
    createdOn         : joi.date().required(),
    updatedOn         : joi.date(),
    deletedOn         : joi.date(),
    reportCount       : joi.number().default(0).description('report abuse count by users'),
    recentlyReportedOn: joi.date().description('recently user reported date'),
    reportedHistory   : joi.array().items(joi.object({
        id  : joi.string().required().description('reported userId'),
        date: joi.date().description('reported date')
    })),
    blockedOn         : joi.date().description('admiun blocked date')
};
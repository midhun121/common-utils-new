/**
 * Created by hmspl on 25/8/16.
 */
var joi = require("joi");

module.exports = {
    id         : joi.string().required(),
    dbType     : joi.string().default("promoCode"),
    code       : joi.string().required(),
    points     : joi.number().default(10).required(),
    status     : joi.string().required().valid(["ACTIVE", "INACTIVE", "DELETED"]),
    createdDate: joi.date(),
    usedDetails: joi.array().items(joi.object({
        userId: joi.string().required(),
        usedOn: joi.date()
    })),
    updatedDate: joi.date(),
    deletedDate: joi.date()
};
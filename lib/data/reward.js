var joi = require("joi");

module.exports = {
    id                : joi.string().required(),
    dbType            : joi.string().default("reward"),
    productId         : joi.string().description("Required only for SHARED type"),
    brandId           : joi.string().required(),
    rewardType        : joi.string().required().valid(["SHARED", "BRAND", "SOCIAL"]),
    rewardName        : joi.string().required(),
    label             : joi.string().description("Required only for SHARED type"),
    speedDial         : joi.number(),
    price             : joi.number().default(0),
    allocated         : joi.number().default(0),
    redeemed          : joi.number().default(0),
    redemptionLimit   : joi.number(),
    allocatedCnt      : joi.number().default(0),
    redeemedCnt       : joi.number().default(0),
    credit            : joi.number().default(0),
    desc              : joi.string(),
    subDesc           : joi.string(),
    termsAndConditions: joi.string(),
    tags              : joi.string(),
    status            : joi.string().required().valid(["ACTIVE", "INACTIVE", "DELETED"]),
    imageKey          : joi.string(),
    createdDate       : joi.date().required(),
    updatedDate       : joi.date(),
    deletedDate       : joi.date(),

    rewardSubType  : joi.string().valid(["PROMO_DISCOUNT", "REDEEM"]),
    promoCode      : joi.string(),
    earnPoints     : joi.number(),
    offerDetails   : joi.object({
        spend   : joi.number().required().description('Spend Amount'),
        discount: joi.number().description('Discount value in Percentage'),
        save    : joi.number().description('Save value in Amount')
    }),
    isRedeemInStore: joi.boolean().description('Redeem In Store/ Venue')
};
/**
 * Created by jaya on 30/05/16.
 */

var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("session"),
    id         : joi.string().description("Unique user id").required(),
    tokens     : joi.array().items(joi.string()).description("Token that are in active"),
    createdDate: joi.date().required(),
    updatedDate: joi.date()
};
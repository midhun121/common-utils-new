var joi = require('joi');

module.exports = {
    dbType     : joi.string().default("twitter"),
    id         : joi.string().description("Unique id of the document. That is same as fb user Id").required(),
    userId     : joi.string().description("Unique id for the user").required(),
    createdDate: joi.string().required(),
    updatedDate: joi.string()
};

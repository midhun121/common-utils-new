var joi = require("joi");

module.exports = {
    id             : joi.string().required().description("make id as userId:brandId"),
    dbType         : joi.string().default("userBrandMap"),
    userId         : joi.string().required(),
    brandId        : joi.string().required(),
    isFollow       : joi.boolean().required().default(false),
    redeemedPoints : joi.number().default(0),
    allocatedPoints: joi.number().default(0),

    sharedAllocatedPoints: joi.number().default(0),
    socialAllocatedPoints: joi.number().default(0),
    brandAllocatedPoints : joi.number().default(0),
    sharedRedeemedPoints : joi.number().default(0),
    socialRedeemedPoints : joi.number().default(0),
    brandRedeemedPoints  : joi.number().default(0),

    visitCount        : joi.number().default(0),
    visitedDate       : joi.array().items(joi.date().required()),
    recentRedeem      : joi.object({
        rewardId  : joi.string(),
        points    : joi.number().required(),
        redeemedOn: joi.date().required()
    }),
    recentAllocated   : joi.object({
        rewardId   : joi.string(),
        points     : joi.number().required(),
        allocatedOn: joi.date().required()
    }),
    oldRecentRedeem   : joi.object({
        rewardId  : joi.string(),
        points    : joi.number().required(),
        redeemedOn: joi.date().required()
    }),
    oldRecentAllocated: joi.object({
        rewardId   : joi.string(),
        points     : joi.number().required(),
        allocatedOn: joi.date().required()
    }),
    createdDate       : joi.date().required(),
    updatedDate       : joi.date()
};
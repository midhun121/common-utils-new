/**
 * Created by jaya on 30/05/16.
 */

var joi = require('joi');

module.exports = {
    dbType  : joi.string().default("userDeviceMap"),
    id      : joi.string().description("User Id").required(),
    tokens  : joi.object().description("This object will have the key as device type and value as object contains deviceId and deviceToken"),
    userType: joi.string().valid(['USER', 'BRAND']),

    /** object will be look like this

     tokens: {
        $$deviceType$$: {
            "deviceId":{
                deviceId: "****",
                deviceToken: "######"
            }
        }
    }
     */

    createdDate: joi.date().required(),
    updatedDate: joi.date()
};
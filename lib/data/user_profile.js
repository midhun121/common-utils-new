var joi = require("joi");

module.exports = {
    id                        : joi.string().required(),
    dbType                    : joi.string().default("userProfile"),
    name                      : joi.object({
        firstName: joi.string().required(),
        lastName : joi.string()
    }),
    gender                    : joi.string(),
    dob                       : joi.date(),
    email                     : joi.string(),
    postCode                  : joi.number(),
    facebook                  : joi.object({
        fbId    : joi.string().required(),
        fbToken : joi.string().required(),
        email   : joi.string(),
        username: joi.string()
    }),
    twitter                   : joi.object({
        twitterId   : joi.string().required(),
        twitterToken: joi.string().required(),
        email       : joi.string(),
        username    : joi.string()
    }),
    instagram                 : joi.object({
        instagramId   : joi.string(),
        instagramToken: joi.string(),
        email         : joi.string(),
        username      : joi.string()
    }),
    mail                      : joi.object({
        email   : joi.string().required(),
        password: joi.string().required()
    }),
    imageKey                  : joi.string().description("Image Key"),
    status                    : joi.string().required().valid(["ACTIVE", "INACTIVE", "DELETED"]),
    notifyRewardsAndPromotions: joi.boolean().required().default(true),
    allocatedPoints           : joi.number().required().default(0),
    redeemedPoints            : joi.number().required().default(0),
    pointsBalance             : joi.number(),

    /*sharedAllocatedPoints: joi.number().default(0),
     socialAllocatedPoints: joi.number().default(0),
     brandAllocatedPoints : joi.number().default(0),
     sharedRedeemedPoints : joi.number().default(0),
     socialRedeemedPoints : joi.number().default(0),
     brandRedeemedPoints  : joi.number().default(0),*/

    recentRedeem      : joi.object({
        rewardId  : joi.string(),
        brandId   : joi.string(),
        points    : joi.number().required(),
        redeemedOn: joi.date().required()
    }),
    recentAllocated   : joi.object({
        rewardId   : joi.string(),
        brandId    : joi.string(),
        points     : joi.number().required(),
        allocatedOn: joi.date().required()
    }),
    oldRecentRedeem   : joi.object({
        rewardId  : joi.string(),
        brandId   : joi.string(),
        points    : joi.number().required(),
        redeemedOn: joi.date().required()
    }),
    oldRecentAllocated: joi.object({
        rewardId   : joi.string(),
        brandId    : joi.string(),
        points     : joi.number().required(),
        allocatedOn: joi.date().required()
    }),
    referralCode      : joi.string(),
    referralCodeUsed  : joi.array().items(joi.string()),
    referralBy        : joi.array().items(joi.string()),
    createdDate       : joi.date().required(),
    updatedDate       : joi.date(),
    deletedDate       : joi.date(),
    recentBeaconIds   : joi.array().items(joi.string()),
    facebookUrl       : joi.string(),
    twitterUrl        : joi.string(),
    instagramUrl      : joi.string(),
    setupCompleted    : joi.boolean().default(false)

};
var joi = require("joi");

module.exports = {
    id     : joi.string().required(),
    dbType : joi.string().default("Beacon"),
    type   : joi.string().required().description("Beacon"),
    brandId: joi.string().required(),
    data   : joi.object(),
    oldData: joi.object().description('contains recent walk out data')
};

/*$$userId: joi.object({
 id: joi.string().required(),
 visitedOn: joi.date().required(),
 pointsHistory: joi.string().required().description("points history id")
 })*/

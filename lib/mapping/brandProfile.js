module.exports = {
    "properties": {
        "adminPoints"              : {
            "type": "long"
        },
        "adminPointsHistory"       : {
            "type": "long"
        },
        "allocatedPoints"          : {
            "type": "long"
        },
        "balancePoints"            : {
            "type": "long"
        },
        "beaconId"                 : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "brandAllocatedPoints"     : {
            "type": "long"
        },
        "brandRedeemedPoints"      : {
            "type": "long"
        },
        "businessName"             : {
            "type"  : "string",
            "index" : "not_analyzed",
            "fields": {
                "businessNameString": {
                    "type": "string"
                }
            }
        },
        "createdDate"              : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "credit"                   : {
            "type": "long"
        },
        "creditValue"              : {
            "type": "long"
        },
        "deletedDate"              : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "desc"                     : {
            "type": "string"
        },
        "description"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "email"                    : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "facebook"                 : {
            "properties": {
                "email"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "fbId"    : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "fbToken" : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username": {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "facebookUrl"              : {
            "type": "string"
        },
        "id"                       : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageKey"                 : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageURL"                 : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "images"                   : {
            "type": "string"
        },
        "instagram"                : {
            "properties": {
                "email"         : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "instagramId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "instagramToken": {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username"      : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "instagramUrl"             : {
            "type": "string"
        },
        "location"                 : {
            "properties": {
                "address"    : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "geoLocation": {
                    "type": "geo_point"
                }
            }
        },
        "mail"                     : {
            "properties": {
                "email"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "password": {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "notificationDisabledUsers": {
            "type": "string"
        },
        "openHours"                : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "openingBalance"           : {
            "type": "long"
        },
        "openingSocialBrandBalance": {
            "type": "long"
        },
        "paidAmountHistory"        : {
            "type": "long"
        },
        "redeemedPoints"           : {
            "type": "long"
        },
        "setupCompleted"           : {
            "type": "boolean"
        },
        "sharedAllocatedPoints"    : {
            "type": "long"
        },
        "sharedPoints"             : {
            "type": "long"
        },
        "sharedRedeemedPoints"     : {
            "type": "long"
        },
        "socialAllocatedPoints"    : {
            "type": "long"
        },
        "socialBrandPoints"        : {
            "type": "long"
        },
        "socialRedeemedPoints"     : {
            "type": "long"
        },
        "status"                   : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "telephone"                : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "thumbnailImageKey"        : {
            "type": "string"
        },
        "totalPaidAmount"          : {
            "type": "long"
        },
        "twitter"                  : {
            "properties": {
                "email"       : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "twitterId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "twitterToken": {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username"    : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "twitterUrl"               : {
            "type": "string"
        },
        "type"                     : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "typeOfBusiness"           : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedDate"              : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "website"                  : {
            "type" : "string",
            "index": "not_analyzed"
        }
    }
};
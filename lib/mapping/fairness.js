/**
 * Created by hmspl on 18/5/16.
 */
module.exports = {
    "properties": {
        "adjustedNetValue"             : {
            "type": "long"
        },
        "allocated"                    : {
            "type": "long"
        },
        "allocatedCount"               : {
            "type": "long"
        },
        "allocatedValue"               : {
            "type": "long"
        },
        "avgPBap"                      : {
            "type": "long"
        },
        "brandId"                      : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "createdOn"                    : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "credit"                       : {
            "type": "long"
        },
        "creditBalance"                : {
            "type": "long"
        },
        "creditBalanceWithRRP"         : {
            "type": "long"
        },
        "currentBalance"               : {
            "type": "long"
        },
        "differenceInAllocation"       : {
            "type": "long"
        },
        "differenceInRRP"              : {
            "type": "long"
        },
        "differenceInRRPAgianstPBaP"   : {
            "type": "long"
        },
        "finalAvgPBap"                 : {
            "type": "long"
        },
        "finalFairnessValue"           : {
            "type": "long"
        },
        "finalPoint"                   : {
            "type": "long"
        },
        "hiddenValue"                  : {
            "type": "long"
        },
        "id"                           : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "isConsidered"                 : {
            "type": "boolean"
        },
        "netPoints"                    : {
            "type": "long"
        },
        "openingBalance"               : {
            "type": "long"
        },
        "price"                        : {
            "type": "long"
        },
        "productId"                    : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "redeemed"                     : {
            "type": "long"
        },
        "redeemedCount"                : {
            "type": "long"
        },
        "redeemedValue"                : {
            "type": "long"
        },
        "rewardId"                     : {
            "type": "string"
        },
        "rrpNetPoints"                 : {
            "type": "long"
        },
        "status"                       : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "totalPointsAllocated"         : {
            "type": "long"
        },
        "totalPointsAllocatedToPeberry": {
            "type": "long"
        },
        "totalPointsRedeemed"          : {
            "type": "long"
        },
        "totalPointsRedeemedToPeberry" : {
            "type": "long"
        },
        "type"                         : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedFairnessValue"         : {
            "type": "long"
        },
        "updatedOn"                    : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "volume"                       : {
            "type": "long"
        },
        "weightedAgainstPrice"         : {
            "type": "long"
        }
    }
};
/**
 * Created by hmspl on 30/11/16.
 */
module.exports = {
    "properties": {
        "brandId"  : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "createdOn": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "dbType"   : {
            "type": "string"
        },
        "id"       : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "isRead"   : {
            "type": "boolean"
        },
        "message"  : {
            "type": "string"
        },
        "notiType" : {
            "type": "string"
        },
        "status"   : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedOn": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "userId"   : {
            "type" : "string",
            "index": "not_analyzed"
        }
    }
};
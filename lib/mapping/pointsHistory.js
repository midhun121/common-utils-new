module.exports = {
    "properties": {
        "brandId"    : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "createdOn"  : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "extraPoints": {
            "type": "long"
        },
        "id"         : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "points"     : {
            "type": "long"
        },
        "quantity"   : {
            "type": "long"
        },
        "rewardId"   : {
            "type": "string"
        },
        "rewardType" : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "type"       : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "userId"     : {
            "type" : "string",
            "index": "not_analyzed"
        }
    }
};
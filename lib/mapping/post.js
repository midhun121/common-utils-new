module.exports = {
    "properties": {
        "blockedOn"         : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "brandId"           : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "createdDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "createdOn"         : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "deletedDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "id"                : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageKey"          : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "postMessage"       : {
            "type"  : "string",
            "index" : "not_analyzed",
            "fields": {
                "postMessageString": {
                    "type": "string"
                }
            }
        },
        "recentlyReportedOn": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "reportCount"       : {
            "type": "long"
        },
        "reportedHistory"   : {
            "properties": {
                "date": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "id"  : {
                    "type": "string"
                }
            }
        },
        "status"            : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "type"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "updatedOn"         : {
            "type"  : "date",
            "format": "dateOptionalTime"
        }
    }
};
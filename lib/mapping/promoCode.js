/**
 * Created by hmspl on 30/11/16.
 */
module.exports = {
    "properties": {
        "code"       : {
            "type": "string"
        },
        "createdDate": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "dbType"     : {
            "type": "string"
        },
        "deletedDate": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "id"         : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "points"     : {
            "type": "long"
        },
        "status"     : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedDate": {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "usedDetails": {
            "properties": {
                "usedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "userId": {
                    "type": "string"
                }
            }
        }
    }
}
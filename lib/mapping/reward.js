module.exports = {
    "properties": {
        "allocated"         : {
            "type": "long"
        },
        "allocatedCnt"      : {
            "type": "long"
        },
        "brandId"           : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "createdDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "credit"            : {
            "type": "long"
        },
        "deletedDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "desc"              : {
            "type": "string"
        },
        "description"       : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "earnPoints"        : {
            "type": "long"
        },
        "id"                : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageKey"          : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "isRedeemInStore"   : {
            "type": "boolean"
        },
        "label"             : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "name"              : {
            "properties": {
                "rewardName": {
                    "type"  : "string",
                    "index" : "not_analyzed",
                    "fields": {
                        "rewardNameString": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "offerDetails"      : {
            "properties": {
                "discount": {
                    "type": "long"
                },
                "save"    : {
                    "type": "long"
                },
                "spend"   : {
                    "type": "long"
                }
            }
        },
        "price"             : {
            "type": "long"
        },
        "productId"         : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "promoCode"         : {
            "type": "string"
        },
        "redeemed"          : {
            "type": "long"
        },
        "redeemedCnt"       : {
            "type": "long"
        },
        "redemptionLimit"   : {
            "type": "long"
        },
        "rewardName"        : {
            "type": "string"
        },
        "rewardSubType"     : {
            "type": "string"
        },
        "rewardType"        : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "speedDial"         : {
            "type": "long"
        },
        "status"            : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "subDesc"           : {
            "type": "string"
        },
        "tags"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "termsAndConditions": {
            "type" : "string",
            "index": "not_analyzed"
        },
        "type"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "updatedDate"       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        }
    }
};
module.exports = {
    "properties": {
        "allocatedPoints"      : {
            "type": "long"
        },
        "brandAllocatedPoints" : {
            "type": "long"
        },
        "brandId"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "brandRedeemedPoints"  : {
            "type": "long"
        },
        "createdDate"          : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "id"                   : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "isFollow"             : {
            "type": "boolean"
        },
        "oldRecentAllocated"   : {
            "properties": {
                "allocatedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "points"     : {
                    "type": "long"
                },
                "rewardId"   : {
                    "type": "string"
                }
            }
        },
        "oldRecentRedeem"      : {
            "properties": {
                "points"    : {
                    "type": "long"
                },
                "redeemedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "rewardId"  : {
                    "type": "string"
                }
            }
        },
        "recentAllocated"      : {
            "properties": {
                "allocatedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "points"     : {
                    "type": "long"
                },
                "rewardId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "recentRedeem"         : {
            "properties": {
                "points"    : {
                    "type": "long"
                },
                "redeemedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "rewardId"  : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "redeemedPoints"       : {
            "type": "long"
        },
        "sharedAllocatedPoints": {
            "type": "long"
        },
        "sharedRedeemedPoints" : {
            "type": "long"
        },
        "socialAllocatedPoints": {
            "type": "long"
        },
        "socialRedeemedPoints" : {
            "type": "long"
        },
        "updatedDate"          : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "userId"               : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "visitCount"           : {
            "type": "long"
        },
        "visitedDate"          : {
            "type"  : "date",
            "format": "dateOptionalTime"
        }
    }
};
module.exports = {
    "properties": {
        "allocatedPoints"           : {
            "type": "long"
        },
        "createdDate"               : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "deletedDate"               : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "dob"                       : {
            "type"  : "date",
            "format": "dateOptionalTime"
        },
        "email"                     : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "facebook"                  : {
            "properties": {
                "email"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "fbId"    : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "fbToken" : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username": {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "facebookUrl"               : {
            "type": "string"
        },
        "gender"                    : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "id"                        : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageKey"                  : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "imageURL"                  : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "instagram"                 : {
            "properties": {
                "email"         : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "instagramId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "instagramToken": {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username"      : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "instagramUrl"              : {
            "type": "string"
        },
        "mail"                      : {
            "properties": {
                "email"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "password": {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "name"                      : {
            "properties": {
                "firstName": {
                    "type"  : "string",
                    "index" : "not_analyzed",
                    "fields": {
                        "firstNameString": {
                            "type": "string"
                        }
                    }
                },
                "lastName" : {
                    "type"  : "string",
                    "index" : "not_analyzed",
                    "fields": {
                        "lastNameString": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "notifyRewardsAndPromotions": {
            "type": "boolean"
        },
        "oldRecentAllocated"        : {
            "properties": {
                "allocatedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "brandId"    : {
                    "type": "string"
                },
                "points"     : {
                    "type": "long"
                },
                "rewardId"   : {
                    "type": "string"
                }
            }
        },
        "oldRecentRedeem"           : {
            "properties": {
                "brandId"   : {
                    "type": "string"
                },
                "points"    : {
                    "type": "long"
                },
                "redeemedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "rewardId"  : {
                    "type": "string"
                }
            }
        },
        "pointsBalance"             : {
            "type": "long"
        },
        "postCode"                  : {
            "type": "long"
        },
        "postcode"                  : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "recentAllocated"           : {
            "properties": {
                "allocatedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "brandId"    : {
                    "type": "string"
                },
                "points"     : {
                    "type": "long"
                },
                "rewardId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "recentBeaconIds"           : {
            "type": "string"
        },
        "recentRedeem"              : {
            "properties": {
                "brandId"   : {
                    "type": "string"
                },
                "points"    : {
                    "type": "long"
                },
                "redeemedOn": {
                    "type"  : "date",
                    "format": "dateOptionalTime"
                },
                "rewardId"  : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "redeemedPoints"            : {
            "type": "long"
        },
        "referralBy"                : {
            "type": "string"
        },
        "referralCode"              : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "referralCodeUsed"          : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "setupCompleted"            : {
            "type": "boolean"
        },
        "status"                    : {
            "type" : "string",
            "index": "not_analyzed"
        },
        "twitter"                   : {
            "properties": {
                "email"       : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "twitterId"   : {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "twitterToken": {
                    "type" : "string",
                    "index": "not_analyzed"
                },
                "username"    : {
                    "type" : "string",
                    "index": "not_analyzed"
                }
            }
        },
        "twitterUrl"                : {
            "type": "string"
        },
        "updatedDate"               : {
            "type"  : "date",
            "format": "dateOptionalTime"
        }
    }
};
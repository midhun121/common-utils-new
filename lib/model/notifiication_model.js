/**
 * Created by safi on 26/04/15 5:55 PM.
 */

var apnService = require('../service/apn');
var gcmService = require('../service/gcm');
var _ = require('underscore');
var joi = require('joi');

var external = {};

/**Send Push Notification
 *
 * @param {Object} notiObj - {"userId":"","mode":"","badge": number ,"deviceToken":[],"message":""}
 * @param callback - optional
 */
external.sendNotificationNow = function (notiObj, callback) {
    if (!callback)
        callback = function () {
        };

    if (notiObj.mode.toUpperCase() == 'IOS') {
        var deviceIds = notiObj.deviceToken;
        for (var i = 0; i < deviceIds.length; i++) {

            var msgObj = {
                deviceToken: deviceIds[i],
                message: notiObj.message,
                badge: notiObj.badge ? parseInt(notiObj.badge) : 1
            };
            //console.log(msgObj);
            //APN notification call
            apnService.sendMessage(msgObj)
        }
    }
    else if (notiObj.mode.toUpperCase() == 'ANDROID') {
        var msgObject = notiObj.message;
        var registrationIds = notiObj.deviceToken;
        //gcm notification call
        gcmService.sendMessage(msgObject, registrationIds)
    }
};

module.exports = external;
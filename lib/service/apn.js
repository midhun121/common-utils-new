/**
 * Created by safi on 26/04/15 6:33 PM.
 */
var join     = require('path').join;
var Apnagent = require('apnagent');

var appConfig = require('../config/app');


var apn = function () {
    var external = {};

// Locate your certificate
    var userPfx  = join(__dirname, appConfig.notification.ios.userCertPath);
    var brandPfx = join(__dirname, appConfig.notification.ios.brandCertPath);

// Create a new agent
    var brandAgent = new Apnagent.Agent();
    var userAgent  = new Apnagent.Agent();

// set our credentials
    brandAgent.set('pfx file', brandPfx);
    brandAgent.set('passphrase', appConfig.notification.ios.passphrase);

// our credentials were for development
    brandAgent.enable(appConfig.notification.ios.mode);

    brandAgent.connect(function (err) {
        // gracefully handle auth problems
        if (err && err.name === 'GatewayAuthorizationError') {
            console.log('Authentication Error: %s', err.message);
            process.exit(1);
        }
        // handle any other err (not likely)
        else if (err) {
            throw err;
        }
        // it worked!
        var env = brandAgent.enabled('sandbox')
            ? 'sandbox'
            : 'production';
        console.log('brand Agent [%s] gateway connected', env);
    });

// set our credentials
    userAgent.set('pfx file', userPfx);
    userAgent.set('passphrase', appConfig.notification.ios.passphrase);

// our credentials were for development
    userAgent.enable(appConfig.notification.ios.mode);

    userAgent.connect(function (err) {
        // gracefully handle auth problems
        if (err && err.name === 'GatewayAuthorizationError') {
            console.log('Authentication Error: %s', err.message);
            process.exit(1);
        }
        // handle any other err (not likely)
        else if (err) {
            throw err;
        }
        // it worked!
        var env = brandAgent.enabled('sandbox')
            ? 'sandbox'
            : 'production';
        console.log('user Agent [%s] gateway connected', env);
    });

    /** APN send Notification IOS
     *
     * @param {Object} msgObj - required
     */
    external.sendMessageForBrand = function (msgObj, deviceToken) {
        //console.log(msgObj);
        console.log('FINAL BRAND ', msgObj, deviceToken);
        if (msgObj.msg && msgObj.msg.text) {
            brandAgent.createMessage()
                .device(deviceToken)
                .alert(msgObj.msg.text)
                .badge(msgObj.badge)
                .set('data', msgObj)
                .send();
        }
        brandAgent.on('message:error', function (err, msg) {
            console.log('apn err ', err, deviceToken);

            switch (err.name) {
                // This error occurs when Apple reports an issue parsing the message.
                case 'GatewayNotificationError':
                    console.log('[message:error] GatewayNotificationError: %s', err.message);

                    // The err.code is the number that Apple reports.
                    // Example: 8 means the token supplied is invalid or not subscribed
                    // to notifications for your application.
                    if (err.code === 8) {
                        console.log('    > %s', msg.device().toString());
                        // In production you should flag this token as invalid and not
                        // send any futher messages to it until you confirm validity
                    }

                    break;

                // This happens when apnagent has a problem encoding the message for transfer
                case 'SerializationError':
                    console.log('[message:error] SerializationError: %s', err.message);
                    break;

                // unlikely, but could occur if trying to send over a dead socket
                default:
                    console.log('[message:error] other error: %s', err.message);
                    break;
            }
        });
    };

    /** APN send Notification IOS
     *
     * @param {Object} msgObj - required
     */
    external.sendMessageForUser = function (msgObj, deviceToken) {
        //console.log(msgObj);
        console.log('FINAL USER ', msgObj, deviceToken);
        if (msgObj.msg && msgObj.msg.text) {
            userAgent.createMessage()
                .device(deviceToken)
                .alert(msgObj.msg.text)
                .set('data', msgObj)
                .send();
        }
        userAgent.on('message:error', function (err, msg) {
            console.log('err ', err, deviceToken);

            switch (err.name) {
                // This error occurs when Apple reports an issue parsing the message.
                case 'GatewayNotificationError':
                    console.log('[message:error] GatewayNotificationError: %s', err.message);

                    // The err.code is the number that Apple reports.
                    // Example: 8 means the token supplied is invalid or not subscribed
                    // to notifications for your application.
                    if (err.code === 8) {
                        console.log('    > %s', msg.device().toString());
                        // In production you should flag this token as invalid and not
                        // send any futher messages to it until you confirm validity
                    }

                    break;

                // This happens when apnagent has a problem encoding the message for transfer
                case 'SerializationError':
                    console.log('[message:error] SerializationError: %s', err.message);
                    break;

                // unlikely, but could occur if trying to send over a dead socket
                default:
                    console.log('[message:error] other error: %s', err.message);
                    break;
            }
        });
    };

    return external;
};

module.exports = apn;
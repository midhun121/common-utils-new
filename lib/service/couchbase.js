/**
 * Created by jaya on 23/08/15.
 */
var couchbase = {};

/**
 * Get document from couchbase
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param callback
 */
couchbase.get = function (DB, key, callback) {
    DB.get(key, callback);
};

/**
 *
 * @param DB - DB
 * @param key - Couchbase Key
 * @param count - Increment / Decrement count
 * @param options -  option - Object
 * @param callback - callback
 */
couchbase.counter = function (DB, key, count, options, callback) {
    DB.counter(key, count, options, callback);
};

/**
 * Insert document to couchbase
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param {any} value Required - value for the document key
 * @param callback
 */
couchbase.insert = function (DB, key, value, callback) {
    DB.insert(key, value, callback);
};

/**
 * Update document to couchbase
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param {any}  value Required - value for the document key
 * @param {Object} options
 *  @param [options.cas=undefined]
 *  The CAS value to check. If the item on the server contains a different
 *  CAS value, the operation will fail.  Note that if this option is undefined,
 *  no comparison will be performed.
 *  @param [options.expiry=0]
 *  Set the initial expiration time for the document.  A value of 0 represents
 *  never expiring.
 *  @param [options.persist_to=0]
 *  Ensures this operation is persisted to this many nodes
 *  @param [options.replicate_to=0]
 *  Ensures this operation is replicated to this many nodes
 * @param callback
 */
couchbase.replace = function (DB, key, value, options, callback) {

    if (options instanceof Function) {
        callback = arguments[3];
        options  = {};
    }

    DB.replace(key, value, options, callback);
};

/**
 * Update document to couchbase
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param {any} value Required - value for the document key
 * @param {Object} options Optional
 *  @param [options.cas=undefined]
 *  The CAS value to check. If the item on the server contains a different
 *  CAS value, the operation will fail.  Note that if this option is undefined,
 *  no comparison will be performed.
 *  @param [options.expiry=0]
 *  Set the initial expiration time for the document.  A value of 0 represents
 *  never expiring.
 *  @param [options.persist_to=0]
 *  Ensures this operation is persisted to this many nodes
 *  @param [options.replicate_to=0]
 *  Ensures this operation is replicated to this many nodes
 * @param callback
 */
couchbase.upsert = function (DB, key, value, options, callback) {
    if (options instanceof Function) {
        callback = arguments[3];
        options  = {};
    }

    DB.upsert(key, value, options, callback);
};

/**
 * Remove document from database
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param callback
 */
couchbase.remove = function (DB, key, callback) {
    DB.remove(key, callback);
};

/**
 * Get Multi documents from database
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {Array.<Buffer|string>} keys - Target document keys
 * @param callback
 */
couchbase.getMulti = function (DB, keys, callback) {
    DB.getMulti(keys, callback);
};

/**
 * Retrieves a document and updates the expiry of the item at the same time.
 *
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string|Buffer} key Required - The target document key.
 *
 * The expiration time to use. If a value of 0 is provided, then the
 * current expiration time is cleared and the key is set to
 * never expire. Otherwise, the key is updated to expire in the
 * time provided (in seconds).
 * @param [options] {Object}
 * @param callback
 */
couchbase.getAndTouch = function (DB, key, expiry, options, callback) {
    if (options instanceof Function) {
        callback = arguments[3];
        options  = {};
    }

    DB.getAndTouch(key, expiry, options, callback);
};

/*View query will get the list of result with the query*/
couchbase.viewQuery = function (DB, query, callback) {
    DB.query(query, callback);
};

/**
 * Get from couchbase and lock document for the given time.
 * @param {Object} DB Required - Singleton object. That contains couchbase db instance
 * @param {string} key Required - document key
 * @param {object} lockTime Object Required - Eg:{lockTime : 10}
 * @param callback - required
 */
couchbase.getAndLock = function (DB, key, lockTimeObj, callback) {
    DB.getAndLock(key, lockTimeObj, callback);
};

module.exports = couchbase;
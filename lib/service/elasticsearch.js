/**
 * Created by jaya on 23/08/15.
 */

var elasticsearch = {};

/**
 * Insert the document to Elastic search
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param {Object} body - Data to persist es
 * @param callback
 */
elasticsearch.create = function (ES, indexData, body, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        id   : indexData.id,
        body : body
    };

    ES.create(params, callback);
};

/**
 * Check is there any document is available with this index
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param callback
 */
elasticsearch.exists = function (ES, indexData, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        id   : indexData.id
    };

    ES.exists(params, callback);
};

/**
 *
 * Update the document to Elastic search
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param {Object} body - Data to persist es
 * @param callback
 */
elasticsearch.update = function (ES, indexData, body, callback) {
    if (!callback)
        callback = function () {
        };
    elasticsearch.exists(ES, indexData, function (err, result) {
        if (result) {
            elasticsearch.delete(ES, indexData, function (err, result) {
                if (err) {
                    return callback(err);
                } else {
                    elasticsearch.upsert(ES, indexData, body, function (err, res) {
                        if (err) {
                            return callback(err);
                        } else {
                            return callback();
                        }
                    });
                }
            });
        } else {
            elasticsearch.upsert(ES, indexData, body, function (err, res) {
                if (err) {
                    return callback(err);
                } else {
                    return callback();
                }
            });
        }
    });

};

/**
 * Update the document to Elastic search if not exists already then insert the document to Elastic search
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param {Object} body - Data to persist es
 * @param callback
 */
elasticsearch.upsert = function (ES, indexData, body, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        id   : indexData.id,
        body : {
            doc          : body,
            doc_as_upsert: true
        }
    };

    ES.update(params, callback);
};

/**
 * Get count from elastic search for particular index and type
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param callback
 */
elasticsearch.count = function (ES, indexData, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        body : indexData.body
    };

    ES.count(params, callback);
};

/**
 * Search documents from elastic search
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param {Object} query - elastic search query
 * @param callback
 */
elasticsearch.search = function (ES, indexData, query, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        body : query
    };

    console.log(JSON.stringify(params));

    ES.search(params, callback);
};

/**
 * Delete document from elastic search
 *
 * @param {Object} ES - elastic search instance
 * @param {Object} indexData - An object with parameters used to carry out this action
 * @param callback
 */
elasticsearch.delete = function (ES, indexData, callback) {
    if (!callback)
        callback = function () {
        };

    var params = {
        index: indexData.index,
        type : indexData.type,
        id   : indexData.id
    };

    ES.delete(params, callback);
};

/**
 *
 * @param ES - elastic search instance
 * @param indexData - Array
 * @param callback - function optional
 */
elasticsearch.bulk = function (ES, indexData, callback) {
    if (!callback)
        callback = function () {
        };

    ES.bulk({body: indexData}, callback);
};

module.exports = elasticsearch;
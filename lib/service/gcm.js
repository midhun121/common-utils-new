/**
 * Created by hmspl on 2/9/15.
 */

var gcm       = require('node-gcm');
var appConfig = require('../config/app');
//var Logger = require('../config/logger');

var GCM = function () {
    var external = {}

    var userSender = new gcm.Sender(appConfig.notification.android.USER_GCM_API_KEY);

    /** GCM Send Notification for Android
     *
     * @param {literal} msgObject - required
     * @param [Array] registrationIds - array of deviceToken
     */
    external.sendMessageForUser = function (msgObject, registrationIds) {

        //Logger.log('info',"msg-->"+JSON.stringify(msgObject));
        //Logger.log('info',"id-->"+registrationIds);

        /*    var message = new gcm.Message({
         collapseKey: 'demo',
         delayWhileIdle: true,
         timeToLive: 3,
         data: {
         sample: 'string data'
         }
         });*/

        var message = new gcm.Message(msgObject);

        /**
         * Params: message-literal, registrationIds-array, No. of retries, callback-function
         **/
        userSender.send(message, registrationIds, 5, function (err, result) {
            console.log('info', result, registrationIds, msgObject);
            console.log('info', "msg--->" + message);
        });
    };

    var brandSender = new gcm.Sender(appConfig.notification.android.BRAND_GCM_API_KEY);

    /** GCM Send Notification for Android
     *
     * @param {literal} msgObject - required
     * @param [Array] registrationIds - array of deviceToken
     */
    external.sendMessageForBrand = function (msgObject, registrationIds) {

        //Logger.log('info',"msg-->"+JSON.stringify(msgObject));
        //Logger.log('info',"id-->"+registrationIds);

        /*    var message = new gcm.Message({
         collapseKey: 'demo',
         delayWhileIdle: true,
         timeToLive: 3,
         data: {
         sample: 'string data'
         }
         });*/

        var message = new gcm.Message(msgObject);

        /**
         * Params: message-literal, registrationIds-array, No. of retries, callback-function
         **/
        brandSender.send(message, registrationIds, 5, function (err, result) {
            console.log(' gcm info', result, registrationIds, msgObject);
            console.log(' gcm info', "msg--->", message);
        });
    };
    return external;
}

module.exports = GCM;
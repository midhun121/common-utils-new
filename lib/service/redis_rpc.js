/**
 * Created by jaya on 27/08/15.
 */

var redisRpc = {};

/**
 * Node Rpc Event Emitter.
 *
 * @param {Object} rpcInstance - RPC Instance
 * @param {string} channelName - RpcChannelName
 * @param {Object} request - Request data
 * @param callback
 */
redisRpc.eventEmitter = function (rpcInstance, channelName, request, callback) {
    rpcInstance.emit(
        channelName,
        request,
        callback);
};

module.exports = redisRpc;
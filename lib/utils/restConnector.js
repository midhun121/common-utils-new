/**
 * Created by jaya on 08/01/16.
 */

var qs      = require('querystring');
var handles = {
    http : require('http'),
    https: require('https')
};

var restConnector = {};

function makeReqParams(params) {
    params        = params || {};
    var reqParams = {
        method  : params.method || 'GET',
        protocol: params.host.protocol + ':',
        hostname: params.host.host,
        port    : params.host.port,
        path    : (params.host.path || '') + (params.path || ''),
        headers : params.headers
    };

    if (!reqParams.path) {
        reqParams.path = '/';
    }

    var query = params.query;
    if (query) {
        reqParams.path = reqParams.path + '?' + qs.stringify(query);
    }

    return reqParams;
}

restConnector.request = function (params, callback) {

    if (!params.host.protocol) {
        params.host.protocol = 'http';
    }

    var reqParams = makeReqParams(params);
    var request;

    var connector = handles[params.host.protocol];
    console.log(reqParams);
    request       = connector.request(reqParams, function (_incoming) {

        var statusCode = _incoming.statusCode;
        var response   = '';
        var headers    = _incoming.headers;

        _incoming.setEncoding('utf8');

        _incoming.on('data', function (data) {
            response += data;
        });

        _incoming.on('error', function (err) {
            console.log('message', JSON.stringify(err));
            return callback(err);
        });
        _incoming.on('end', function () {
            var responseObject = JSON.parse(response);
            return callback(null, statusCode, responseObject, headers);
        });
    });

    request.on('error', function (err) {
        //console.log('message', err.message);
        //console.log(JSON.stringify(err));
        return callback(err);
    });

    if (params.payload) {
        console.log(params.payload)
        request.setHeader('Content-Type', 'application/json');
        request.write(JSON.stringify(params.payload));
    }

    request.end();
};

module.exports = restConnector;
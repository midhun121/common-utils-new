var utilities    = {};
var Cryptr       = require("cryptr");
var moment       = require('moment');
var cryptr       = new Cryptr('hakuna');
var crypto       = require('crypto');
var Boom         = require('boom');
var RES_MSG      = require('../constants/brand_response_messages');
var joi          = require('joi');
var SimpleRandom = require('simple-random');
var async        = require('async');
var _            = require('underscore');
var shortId      = require('shortid');

/**
 * Clone object. Copy data without reference.
 *
 * @param {Object} object
 * returns {Object}
 */
utilities.cloneObject = function (object) {
    return JSON.parse(JSON.stringify(object));
};

/**
 * Generate ShortId
 * @returns {*}
 */
utilities.shortId = function () {
    return shortId.generate();
};

/**
 * Encrypt the string
 * @param {string} code - raw string to encrypt
 * @returns {*}
 */
utilities.encryptString = function (code) {
    try {
        return cryptr.encrypt(code);
    } catch (ex) {
        return "";
    }
};

/**
 * Decrypt the string
 * @param {string} code - raw string to decrypt
 * @returns {*}
 */
utilities.decryptString = function (code) {
    try {
        return cryptr.decrypt(code);
    } catch (ex) {
        return "";
    }
};

/**
 * Build success response object with defined keys
 * @param successObj {object} Required
 * @returns {{statusCode: *, message: *}}
 */
utilities.buildSuccessResponse = function (successObj) {
    var response = {
        "statusCode": successObj.code,
        "message"   : successObj.message
    };
    return response;
};

/**
 * Build Custom Error response by giving code and message
 * @param {Object} errorObj
 * @returns
 */
utilities.buildErrorResponse = function (errorObj) {
    if (errorObj.code < 400) {
        errorObj.code = 400
    }

    var error            = Boom.create(errorObj.code, errorObj.message);
    error.reformat();
    var obj              = {
        statusCode: errorObj.code,
        message   : errorObj.message
    };
    error.output.payload = obj;
    //delete error.output.payload.message;
    delete error.output.payload.error;
    //delete error.output.payload.statusCode;
    return error;

};

/**
 * Dynamic method to invoke model from controller.
 *
 * @param {Object} controllerRequest - Controller Request Data
 * @param callback
 */
utilities.modelMethodInvoker = function (controllerRequest, callback) {

    console.log('NAME: ' + controllerRequest.methodName + ' REQUEST: params' + JSON.stringify(controllerRequest.params), ' query :', JSON.stringify(controllerRequest.query), ' payload : ', JSON.stringify(controllerRequest.payload), ' auth: ', controllerRequest.auth);

    var task   = [];
    var reqObj = {};

    if (controllerRequest.requestParamSchema) {
        task.push(function (innerCb) {
            validateSchema(controllerRequest.params, controllerRequest.requestParamSchema, {stripUnknown: true}, function (err, validatedObj) {
                if (!err) {
                    reqObj.params = validatedObj;
                    innerCb();
                } else {
                    if (!err) {
                        err = 'joi validation Error';
                    }
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE: Params' + JSON.stringify(err));
                    return innerCb(RES_MSG.RES_CUSTOM_ERROR(err.toString()));
                }
            })
        });
    } else {
        reqObj.params = controllerRequest.params
    }

    if (controllerRequest.requestQuerySchema) {
        task.push(function (innerCb) {
            validateSchema(controllerRequest.query, controllerRequest.requestQuerySchema, {stripUnknown: true}, function (err, validatedObj) {
                if (!err) {
                    reqObj.query = validatedObj;
                    innerCb();
                } else {
                    if (!err) {
                        err = 'joi validation Error';
                    }
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE: Query' + JSON.stringify(err));
                    return innerCb(RES_MSG.RES_CUSTOM_ERROR(err.toString()));
                }
            })
        });
    } else {
        reqObj.query = controllerRequest.query
    }

    if (controllerRequest.requestPayloadSchema) {
        task.push(function (innerCb) {
            validateSchema(controllerRequest.payload, controllerRequest.requestPayloadSchema, {stripUnknown: true}, function (err, validatedObj) {
                if (!err) {
                    reqObj.payload = validatedObj;
                    innerCb();
                } else {
                    if (!err) {
                        err = 'joi validation Error';
                    }
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE: Payload' + JSON.stringify(err));
                    return innerCb(RES_MSG.RES_CUSTOM_ERROR(err.toString()));
                }
            })
        });
    } else {
        reqObj.payload = controllerRequest.payload
    }

    if (controllerRequest.auth) {
        reqObj.auth = controllerRequest.auth
    }

    async.series(task, function (err, result) {
        if (!err) {
            controllerRequest.method(reqObj, function (err, result) {
                if (!err) {
                    if (controllerRequest.responseSchema) {
                        validateSchema(result, controllerRequest.responseSchema, {allowUnknown: true}, function (err, validatedResponse) {
                            if (!err) {
                                console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(validatedResponse));
                                return callback(null, validatedResponse);
                            } else {
                                console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(err));
                                return callback(RES_MSG.RES_CUSTOM_ERROR(err.toString()));
                            }
                        });
                    } else {
                        console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(result));
                        return callback(null, result);
                    }
                } else {
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(err));
                    return callback(err);
                }
            });
        } else {
            return callback(err);
        }
    });
};

/**
 * Dynamic method to invoke model from controller for Server.
 *
 * @param {Object} controllerRequest - Controller Request Data
 * @param callback
 */
utilities.modelMethodInvokerServer = function (controllerRequest, callback) {

    console.log('NAME: ' + controllerRequest.methodName + ' REQUEST: params' + JSON.stringify(controllerRequest.params), ' query :', JSON.stringify(controllerRequest.query), ' payload : ', JSON.stringify(controllerRequest.payload));
    var reqObj = utilities.cloneObject(controllerRequest);
    delete reqObj.method;
    delete reqObj.requestParamSchema;
    delete reqObj.requestQuerySchema;
    delete reqObj.requestPayloadSchema;
    delete reqObj.responseSchema;
    /* var reqObj = {
     auth      : controllerRequest.auth,
     params    : controllerRequest.params,
     payload   : controllerRequest.payload,
     query     : controllerRequest.query,
     methodName: controllerRequest.methodName
     };*/

    controllerRequest.method(reqObj, function (err, result) {
        if (!err) {
            console.log(result);
            var response = _.extend(utilities.buildSuccessResponse(RES_MSG.RES_SUCCESS), result);
            validateSchema(response, controllerRequest.responseSchema, {stripUnknown: true}, function (err, validatedResponse) {
                if (!err) {
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(validatedResponse));
                    return callback(validatedResponse);
                } else {
                    console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(err));
                    return callback(RES_MSG.RES_CUSTOM_ERROR(err.toString()));
                }
            });
        } else {
            console.log('NAME: ' + controllerRequest.methodName + ', RESPONSE:' + JSON.stringify(err));
            return callback(utilities.buildErrorResponse(err));
        }
    });

};

/**
 * validate it from joi
 * @param {Object} requestObject - Request object
 * @param {Object} SCHEMA
 * @param {Object} options
 * @param callback
 */
function validateSchema(requestObject, SCHEMA, options, callback) {
    if (requestObject) {
        joi.validate(requestObject, SCHEMA, options, callback);
    } else {
        callback(RES_MSG.RES_BAD_REQUEST);
    }
}

/**
 * Generate Random String
 * @param {number} length Optional
 * @param {string} inputChars Optional
 * @returns {*}
 */
utilities.generateRandomString = function (length, inputChars) {
    if (!length || !_.isEmpty(length)) {
        length = 7;
    } else {
        // nothing to do
    }

    if (!inputChars || _.isEmpty(inputChars)) {
        inputChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    } else {
        // nothing to do
    }
    return SimpleRandom({length: length, chars: inputChars, secure: true});
};


/**
 * Generate Salt Password for original password
 * @param password {string} Required
 * @returns {*}
 */
utilities.generateSaltPassword = function (password) {
    if (!password)
        password = '';
    var salt = crypto.createHash('sha1');
    salt.update(password);
    return salt.digest('hex');
};

/**
 *Generate Random number with fixed length
 * @param length {number} Optional
 * @returns {string}
 */
utilities.getRandomNumber = function (length) {
    if (!length) {
        length = 5;
    } else {
        // nothing to do
    }
    var randomNumber = Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1));
    return randomNumber.toString();
};

/**
 * Build dynamic filter query
 *
 * @param payload
 * @param callback
 * @returns {*}
 */
utilities.buildFilterQuery = function (payload, callback) {

    var filterQueries = [];

    var filters = payload.filters, locFilterFlag = false, locationQuery = {};

    for (var index in filters) {
        var shouldFilters  = [];
        var mustNotFilters = [];
        var mustFilters    = [];
        if (filters[index].type == 'RANGE') {
            if (filters[index] && filters[index].range && filters[index].range.from && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from,
                    "lte": filters[index].range.to
                };

                var range = {"range": rangeData};
                filterQueries.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.from) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from
                };

                var range = {"range": rangeData};
                filterQueries.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "lte": filters[index].range.to
                };
                var range                     = {"range": rangeData};
                filterQueries.push(range);
            }
        } else if (filters[index].type == 'FIXED') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }
            }
        } else if (filters[index].type == 'SEARCH') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = "*" + arrayValue[valuedIndex].value.toLowerCase() + "*";

                    var match = {"wildcard": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);

                    } else {
                        mustNotFilters.push(match);

                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }
            }

        } else if (filters[index].type == 'DEFAULT') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }

            }
        } else if (filters[index].type == 'DEFAULT_MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }

                    if (!_.isEmpty(shouldFilters)) {
                        var shouldBoolQuery = {
                            "bool": {
                                "should": shouldFilters
                            }
                        };
                        filterQueries.push(shouldBoolQuery);
                    }

                    if (!_.isEmpty(mustNotFilters)) {
                        var mustNotBoolQuery = {
                            "bool": {
                                "must_not": mustNotFilters
                            }
                        };
                        filterQueries.push(mustNotBoolQuery);
                    }

                }
            }
        } else if (filters[index].type == 'MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        mustFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }

                    if (!_.isEmpty(shouldFilters)) {
                        var mustBoolQuery = {
                            "bool": {
                                "must": mustFilters
                            }
                        };
                        filterQueries.push(mustBoolQuery);
                    }

                    if (!_.isEmpty(mustNotFilters)) {
                        var mustNotBoolQuery = {
                            "bool": {
                                "must_not": mustNotFilters
                            }
                        };
                        filterQueries.push(mustNotBoolQuery);
                    }

                }
            }
        } else if (filters[index].type == 'LOCATION') {
            var locationValue = filters[index].location;
            if (!locationValue.range) {
                locationValue.range = {};
            }
            locationValue.range.from = locationValue.range.from ? locationValue.range.from : 0;
            locationValue.range.to   = locationValue.range.to ? locationValue.range.to : 5;
            locFilterFlag            = true;
            locationQuery            = {
                "geo_distance_range": {
                    "from"                : locationValue.range.from + 'km',
                    "to"                  : locationValue.range.to + 'km',
                    "location.geoLocation": {
                        "lat": locationValue.geoLocation.lat,
                        "lon": locationValue.geoLocation.lon
                    }
                }
            }

        }
    }

    var sortQuery;
    if (payload.sortingKey) {
        sortQuery                     = {};
        sortQuery[payload.sortingKey] = {
            "order": payload.sortBy
        };
    }

    var query;
    if (filterQueries && filterQueries.length > 0) {
        query = {
            "from" : payload.index,
            "size" : payload.limit,
            "query": {
                "bool": {
                    "must": filterQueries
                }
            }
        }
    } else {
        query = {
            "from" : payload.index,
            "size" : payload.limit,
            "query": {
                "match_all": {}
            }
        }
    }

    if (sortQuery) {
        query.sort = [sortQuery]
    }

    if (locFilterFlag) {
        query.filter = locationQuery;
    }

    return callback(null, query);
};

/**
 * Build requirement filter query
 * @param payload
 * @param minimumShouldMatch
 * @param callback
 * @returns {*}
 */
utilities.buildFilterQueryWithMinimumShould = function (payload, minimumShouldMatch, callback) {

    if (minimumShouldMatch instanceof Function) {
        callback           = arguments[1];
        minimumShouldMatch = "80%";
    }

    var filterQueries = [];

    var filters        = payload.filters, locFilterFlag = false, locationQuery = {};
    var shouldFilters  = [];
    var mustNotFilters = [];
    var mustFilters    = [];
    var rangeFilters   = [];

    for (var index in filters) {

        if (filters[index].type == 'RANGE') {
            if (filters[index] && filters[index].range && filters[index].range.from && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from,
                    "lte": filters[index].range.to
                };

                var range = {"range": rangeData};
                rangeFilters.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.from) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from
                };

                var range = {"range": rangeData};
                rangeFilters.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "lte": filters[index].range.to
                };
                var range                     = {"range": rangeData};
                rangeFilters.push(range);
            }
        } else if (filters[index].type == 'FIXED') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }
            }
        } else if (filters[index].type == 'DEFAULT') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }

            }
        } else if (filters[index].type == 'DEFAULT_MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }

                }
            }
        } else if (filters[index].type == 'MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        mustFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }
            }
        } else if (filters[index].type == 'LOCATION') {
            var locationValue = filters[index].location;
            if (!locationValue.range) {
                locationValue.range = {};
            }
            locationValue.range.from = locationValue.range.from ? locationValue.range.from : 0;
            locationValue.range.to   = locationValue.range.to ? locationValue.range.to : 5;
            locFilterFlag            = true;
            locationQuery            = {
                "geo_distance_range": {
                    "from"                : locationValue.range.from + 'km',
                    "to"                  : locationValue.range.to + 'km',
                    "location.geoLocation": {
                        "lat": locationValue.geoLocation.lat,
                        "lon": locationValue.geoLocation.lon
                    }
                }
            }

        }
    }

    if (!_.isEmpty(rangeFilters)) {
        var rangeQueries = {
            "bool": {
                "should": rangeFilters
            }
        };
        filterQueries.push(rangeQueries);
    }

    if (!_.isEmpty(shouldFilters)) {
        var shouldBoolQuery = {
            "bool": {
                "should"              : shouldFilters,
                "minimum_should_match": minimumShouldMatch
            }
        };
        filterQueries.push(shouldBoolQuery);
    }

    if (!_.isEmpty(mustFilters)) {
        var mustBoolQuery = {
            "bool": {
                "must": mustFilters
            }
        };
        filterQueries.push(mustBoolQuery);
    }

    if (!_.isEmpty(mustNotFilters)) {
        var mustNotBoolQuery = {
            "bool": {
                "must_not": mustNotFilters
            }
        };
        filterQueries.push(mustNotBoolQuery);
    }

    var sortQuery;
    if (payload.sortingKey) {
        sortQuery                     = {};
        sortQuery[payload.sortingKey] = {
            "order": payload.sortBy
        };
    }

    var query;
    if (filterQueries && filterQueries.length > 0) {
        query = {
            "from" : payload.index,
            "size" : payload.limit,
            "query": {
                "bool": {
                    "must": filterQueries
                }
            }
        }
    } else {
        query = {
            "from" : payload.index,
            "size" : payload.limit,
            "query": {
                "match_all": {}
            }
        }
    }

    if (sortQuery) {
        query.sort = [sortQuery]
    }

    if (locFilterFlag) {
        query.filter = locationQuery;
    }

    return callback(null, query);
};

utilities.processSearchText = function (text, callback) {
    var intArr        = [], stringArr = [], mustShouldArr = [], dateArr = [];
    var textArr       = text.split(' ');
    var sText         = "";

    _.each(textArr, function (splitText, key) {

        if (typeof splitText == "string") {
            sText = sText + splitText;
            stringArr.push(sText);
            stringArr.push(sText.toUpperCase());
            stringArr.push(sText.toLowerCase());
            sText = sText + " ";
        } else {
            stringArr.push(splitText);
            stringArr.push(splitText.toUpperCase());
            stringArr.push(splitText.toLowerCase());
        }

        if (!isNaN(splitText)) {
            intArr.push(splitText);
        }
        else if (moment(splitText, "YYYY-MM-DD").isValid()) {
            var dateTime = Date.parse(splitText);
            var dateReg  = /^\d{4}([./-])\d{2}\1\d{2}$/;
            if (dateTime && splitText.match(dateReg)) {
                var splitArr = splitText.split('.');
                if (splitArr.length > 1) {
                    splitText = splitArr[0] + "-" + splitArr[1] + "-" + splitArr[2]
                }
                var splitArr1 = splitText.split('/');
                if (splitArr1.length > 1) {
                    splitText = splitArr1[0] + "-" + splitArr1[1] + "-" + splitArr1[2]
                }
                dateArr.push(splitText);
            }
        }
    });
    var respObj       = {};
    respObj.intArr    = intArr;
    respObj.dateArr   = dateArr;
    respObj.stringArr = stringArr;
    return callback(null, respObj);
};

/**
 * Need To give Input Whole Bucket array that we got from ES & Total count of data. Then This would
 * be calculate Limit for every group and it will put limit key in every group.
 * @param bucketObjectArray {object} Required - ES Bucket Array of object
 * @param documentTotal {number} optional - ES total data count
 * @returns {*}
 */
utilities.calculateDynamicGroupLimit = function (bucketObjectArray) {
    var availableCount = 300; // Set Maximum List count Value
    var documentTotal  = 0;
    var allocatedCount = 0;
    var averageCount   = 0;

    if (bucketObjectArray.length > 0 && availableCount > 0) {
        var bucketCountArray = _.pluck(bucketObjectArray, 'doc_count'); // Split every group total count values in array
        documentTotal = _.reduce(bucketCountArray, function (memo, num) { // Get total document count
            if (num)
                return memo + num;
            else
                return memo
        }, 0);

        averageCount = Math.ceil(availableCount / bucketObjectArray.length); // Calculate Average count based on bucket length

        availableCount = averageCount * bucketObjectArray.length;

        for (var index in bucketObjectArray) { // Allocate Limit based on bucket object list count

            var limit = bucketObjectArray[index].doc_count > averageCount ? averageCount : bucketObjectArray[index].doc_count;

            bucketObjectArray[index].limit = limit;
            allocatedCount += limit;
            availableCount -= limit;
        }

        if (allocatedCount < documentTotal) {
            for (var i = 0; availableCount > 0; i = i % bucketObjectArray.length) {
                if (allocatedCount == documentTotal) {
                    break;
                }
                var limit = bucketObjectArray[i].limit;
                var total = bucketObjectArray[i].doc_count;

                if (total > limit) {
                    var unallocated = total - limit;
                    var allocate    = availableCount > unallocated ? unallocated : availableCount;
                    allocatedCount += allocate;
                    availableCount -= allocate;
                }

                i += 1;
            }
            return bucketObjectArray;
        } else {
            return bucketObjectArray;
        }

    } else {
        return bucketObjectArray;
    }
};

utilities.buildFilterWithGroupByResponseQuery = function (payload, count, indexName, typeName, callback) {

    var filterQueries = [];

    var filters = payload.filters;
    for (var index in filters) {
        var shouldFilters  = [];
        var mustNotFilters = [];
        var mustFilters    = [];
        if (filters[index].type == 'RANGE') {
            if (filters[index] && filters[index].range && filters[index].range.from && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from,
                    "lte": filters[index].range.to
                };

                var range = {"range": rangeData};
                filterQueries.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.from) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "gte": filters[index].range.from
                };

                var range = {"range": rangeData};
                filterQueries.push(range);

            } else if (filters[index] && filters[index].range && filters[index].range.to) {
                var rangeData                 = {};
                rangeData[filters[index].key] = {
                    "lte": filters[index].range.to
                };
                var range                     = {"range": rangeData};
                filterQueries.push(range);
            }
        } else if (filters[index].type == 'FIXED') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }
            }
        } else if (filters[index].type == 'SEARCH') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = "*" + arrayValue[valuedIndex].value.toLowerCase() + "*";

                    var match = {"wildcard": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);

                    } else {
                        mustNotFilters.push(match);

                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }
            }

        } else if (filters[index].type == 'DEFAULT') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {
                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }
                }

                if (!_.isEmpty(shouldFilters)) {
                    var shouldBoolQuery = {
                        "bool": {
                            "should": shouldFilters
                        }
                    };
                    filterQueries.push(shouldBoolQuery);
                }

                if (!_.isEmpty(mustNotFilters)) {
                    var mustNotBoolQuery = {
                        "bool": {
                            "must_not": mustNotFilters
                        }
                    };
                    filterQueries.push(mustNotBoolQuery);
                }

            }
        } else if (filters[index].type == 'DEFAULT_MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        shouldFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }

                    if (!_.isEmpty(shouldFilters)) {
                        var shouldBoolQuery = {
                            "bool": {
                                "should": shouldFilters
                            }
                        };
                        filterQueries.push(shouldBoolQuery);
                    }

                    if (!_.isEmpty(mustNotFilters)) {
                        var mustNotBoolQuery = {
                            "bool": {
                                "must_not": mustNotFilters
                            }
                        };
                        filterQueries.push(mustNotBoolQuery);
                    }

                }
            }
        } else if (filters[index].type == 'MUST') {
            var arrayValue = filters[index].value;
            if (!_.isEmpty(arrayValue)) {
                for (var valuedIndex in arrayValue) {

                    var matchData                 = {};
                    matchData[filters[index].key] = arrayValue[valuedIndex].value;
                    var match                     = {"match": matchData};
                    if (arrayValue[valuedIndex].type == 'IN') {
                        mustFilters.push(match);
                    } else {
                        mustNotFilters.push(match);
                    }

                    if (!_.isEmpty(shouldFilters)) {
                        var mustBoolQuery = {
                            "bool": {
                                "must": mustFilters
                            }
                        };
                        filterQueries.push(mustBoolQuery);
                    }

                    if (!_.isEmpty(mustNotFilters)) {
                        var mustNotBoolQuery = {
                            "bool": {
                                "must_not": mustNotFilters
                            }
                        };
                        filterQueries.push(mustNotBoolQuery);
                    }

                }
            }
        }
    }

    var sortData                 = {};
    sortData[payload.sortingKey] = {
        "order": payload.sortBy
    }

    var groupByquery = {};

    var groupByLimitPart = {
        "tops": {
            "top_hits": {
                "size": count,
                "sort": [
                    sortData
                ]
            }
        }
    };

    groupByquery.aggs = groupByLimitPart;

    if (payload.groupBy.indexOf("Date") > -1 || payload.groupBy.indexOf("date") > -1) {
        groupByquery.date_histogram = {
            "field"        : payload.groupBy,
            "interval"     : "day",
            "time_zone"    : timeZoneOffset,
            "missing"      : "2015-01-01",
            "min_doc_count": 1
        }
    } else {
        groupByquery.terms = {
            "field": payload.groupBy,
            "size" : count
        }
    }
    var groupByPart = {
        "groupByData": groupByquery
    }
    if (filterQueries && filterQueries.length > 0) {
        var query = {
            "from" : 0,
            "size" : payload.limit,
            "query": {
                "bool": {
                    "must": filterQueries
                }
            },
            "aggs" : groupByPart
        }
    }
    else {
        var query = {
            "from" : 0,
            "size" : payload.limit,
            "query": {
                "match_all": {}
            },
            "aggs" : groupByPart
        }
    }

    var params = {
        index: indexName,
        type : typeName,
        body : query
    };

    return callback(null, params);
};

module.exports = utilities;